#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define LIM_NOMBRE 30
#define LIM_CARTON 5
#define MAX_NUM 75

typedef struct {
    char nombre[LIM_NOMBRE];
    unsigned int carton[LIM_CARTON][LIM_CARTON];
} datosJugadores;

/*    Funciones de juego    */
void datosPersonales(datosJugadores *ptr, const unsigned int numeroJugadores);
int elegirPatron();
int generarCarton(datosJugadores *ptr, unsigned int numerosRepetidos[MAX_NUM], const unsigned int jugadorActual);

/*    Depuración    */
void DEBUG_imprimirCarton(datosJugadores *ptr, const unsigned int numeroJugadores);
void DEBUG_imprimirMemoriaDinamica(unsigned int *ptr, const unsigned int amplitudDinamica);

/*    Funciones de utilidades    */
void flusher();
//int conversionCharInt(const char c);

int main()
{
    /*    Variables necesarias    */
    unsigned int numeroJugadores;
    unsigned int patronElegido;
    unsigned int i;
    int comprobarRetorno;
    char salida = 'n';
    unsigned int numerosRepetidos[MAX_NUM];
    
    /*    Configuraciones iniciales necesarias    */
    srand(time(NULL));

    /*    Solicitar número de jugadores    */
    printf("Introduce el numero de jugadores: ");
    scanf("%u", &numeroJugadores);
    flusher();

    /*    Solicitar datos personales    */
    datosJugadores players[numeroJugadores];
    datosPersonales(players, numeroJugadores);
    
    /*    Solicitar patrón a usar    */
    patronElegido = elegirPatron();
    if(!patronElegido)
        return 0;
    
    /*    Generar cartón    */
    //Aquí se envía el jugador actual y se genera el cartón por primera vez.
    //También se guardan los números repetidos en el arreglo dinámico.
    for(i=0; i<numeroJugadores; i++)
    {
        comprobarRetorno = generarCarton(players, numerosRepetidos, i);
        if(comprobarRetorno == -1)
            return -2;
    }

    DEBUG_imprimirCarton(players, numeroJugadores);

    return 0;
}

void datosPersonales(datosJugadores *ptr, const unsigned int numeroJugadores)
{
    unsigned int i;

    for(i=0; i<numeroJugadores; i++)
    {
        printf("Jugador %u, introduce tu nombre: ", i+1);
        fgets(ptr[i].nombre, LIM_NOMBRE, stdin);
    }
}

int elegirPatron()
{
    /*    Funcionamiento de la función    */
    /* Retornos:
     *  Lineas: 1 al 19.
     *  Cruz: 20 al 29.
     *  Marco: 30 al 39.
     *  Cubrir todo: 40.
     *
     */
    
    //char salida = 's';
    //unsigned int salida = 0;
    //unsigned int submenu = 0;
    char opcion;
    
    while(1)
    {
        system("clear"); //noportable
        printf("Introduce el modelo del patron:\n");
        printf("1:    < Tipo LINEA >\n");
        printf("2:    < Tipo CRUZ >\n");
        printf("3:    < Tipo MARCO >\n");
        printf("4:    < Cubrir TODO >\n");
        printf("5:    < Salir del juego :( >\n");
        printf("Tu opcion: ");
        scanf("%c", &opcion);
        flusher();
            
        if(opcion < 48 || opcion > 53)
        {
            printf("Opcion no valida. Reintenta nuevamente.\n");
            getchar();
            continue;
        }
        
        //Salida del juego.
        if(opcion == 53)
            return 0;
        
        //Tipo: Cubrir todo.
        if(opcion == 52)
            return 40;
        
        //Tipo (el resto).
        switch(opcion)
        {
            case '1':
                do
                {
                    system("clear"); //noportable
                    printf("Hay distintas formas patron lineal. Elige a tu gusto:\n");
                    printf("1:    < Horizontal >\n");
                    printf("2:    < Vertical >\n");
                    printf("3:    < Diagonal >\n");
                    printf("4:    < Para. Me arrepiento. Dejame seleccionar otro. >\n");
                    printf("Tu opcion: ");
                    scanf("%c", &opcion);
                    flusher();
                    
                    if(opcion == 49)
                        return 1;
                    else if(opcion == 50)
                        return 2;
                    else if(opcion == 51)
                        return 3;
                    
                } while(opcion < 49 || opcion > 52);
                break;
            
            case '2':
                do
                {
                    system("clear"); //noportable
                    printf("Hay distintas formas patron cruz. Elige a tu gusto:\n");
                    printf("1:    < Pequeña >\n");
                    printf("2:    < Grande >\n");
                    printf("3:    < Para. Me arrepiento. Dejame seleccionar otro. >\n");
                    printf("Tu opcion :");
                    scanf("%c", &opcion);
                    flusher();
                    
                    if(opcion == 49)
                        return 20;
                    else if(opcion == 50)
                        return 21;
                    
                } while(opcion < 49 || opcion > 51);
                break;
            
            case '3':
                do
                {
                    system("clear"); //noportable
                    printf("Hay distintas formas patron MARCO. Elige a tu gusto:\n");
                    printf("1:    < Externo >\n");
                    printf("2:    < Interno >\n");
                    printf("3:    < Para. Me arrepiento. Dejame seleccionar otro. >\n");
                    printf("Tu opcion :");
                    scanf("%c", &opcion);
                    flusher();
                    
                    if(opcion == 49)
                        return 30;
                    else if(opcion == 50)
                        return 31;
                    
                } while(opcion < 49 || opcion > 51);
                break;
        }
    }
}

int generarCarton(datosJugadores *ptr, unsigned int numerosRepetidos[MAX_NUM], const unsigned int jugadorActual)
{
    unsigned int soyRandom=0, valorRepetido=0;
    unsigned int y=0, x=0, i=0;
    
    for(x=0; x<LIM_CARTON; x++)
    {
        for(y=0; y<LIM_CARTON; y++)
        {
            if(x == 2 && y == 2)
            {
                ptr[jugadorActual].carton[x][y] = 0;
                continue;
            }
            
            do
            {
                valorRepetido = 0;
                
                if(x==0)
                    soyRandom = rand() % 15 + 1;
                
                else if(x==1)
                {
                    while(1)
                    {
                        soyRandom = rand() % 30 + 1;
                        if(soyRandom < 16)
                            continue;
                        break;
                    }
                }
                
                else if(x==2)
                {
                    while(1)
                    {
                        soyRandom = rand() % 45 + 1;
                        if(soyRandom < 31)
                            continue;
                        break;
                    }
                }
                
                else if(x==3)
                {
                    while(1)
                    {
                        soyRandom = rand() % 60 + 1;
                        if(soyRandom < 46)
                            continue;
                        break;
                    }
                }
                
                else if(x==4)
                {
                    while(1)
                    {
                        soyRandom = rand() % 75 + 1;
                        if(soyRandom < 61)
                            continue;
                        break;
                    }
                }
                
                //Buscar si soyRandom es repetido.
                for(i=0; i < MAX_NUM; i++)
                {
                    if(numerosRepetidos[i] == soyRandom)
                    {
                        valorRepetido = 1;
                        break;
                    }
                }

            } while(valorRepetido);
            
            ptr[jugadorActual].carton[x][y] = soyRandom;
            
        }
    }
    
    return 0;
}

void DEBUG_imprimirCarton(datosJugadores *ptr, const unsigned int numeroJugadores)
{
    unsigned int y=0, x=0, i=0;
    
    system("clear"); //noportable
    
    for(i=0; i<numeroJugadores; i++)
    {
        printf("Jugador %d\n", i+1);
        for(y=0; y<LIM_CARTON; y++)
        {
            printf("\n");
            for(x=0; x<LIM_CARTON; x++)
            {
                printf("%d", ptr[i].carton[y][x]);
                printf("   ");
            }
        }
        printf("\n\n");
    }
}

void DEBUG_imprimirMemoriaDinamica(unsigned int *ptr, const unsigned int amplitudDinamica)
{
    int i=0;
    
    printf("\nRecorriendo la memoria dinámica:\n");
    for(i=0; i<amplitudDinamica; i++)
        printf("%u ", ptr[i]);
    
    printf("\nCantidad: %u\n", amplitudDinamica);
    
    flusher();
    getchar();
}

/*    Utilidades    */

void flusher()
{
    int c;
    while((c = getchar()) != '\n' && c != EOF);
}
